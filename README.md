# Authors: 
Sebastien Benoit, McKinnley Workman

# Project Description: 
    This Project is an appropriate technology toolkit, created to help engineers 
    and non-engineers alike build systems using appropriate technologies.
    This Project is intended to be a web-based interface using Django as a
    development platform.