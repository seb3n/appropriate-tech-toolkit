# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('toolkit', '0002_adding_system_and_its_manager'),
    ]

    operations = [
        migrations.CreateModel(
            name='Component',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('c_type', models.CharField(max_length=30, choices=[(b'bearning', b'bearing'), (b'pole', b'pole')])),
                ('description', models.TextField()),
                ('drawings', models.FileField(upload_to=None)),
                ('assembly', models.ForeignKey(blank=True, to='toolkit.Assembly', null=True)),
                ('system', models.ManyToManyField(to='toolkit.System')),
            ],
        ),
    ]
