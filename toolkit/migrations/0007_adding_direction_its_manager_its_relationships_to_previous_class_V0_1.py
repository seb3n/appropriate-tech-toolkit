# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('toolkit', '0006_adding_method_its_manager_its_relationships_to_previous_class_V0_1'),
    ]

    operations = [
        migrations.CreateModel(
            name='Direction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.CharField(max_length=20, choices=[(b'A', b'assemble'), (b'D', b'disassemble'), (b'M', b'maintain')])),
                ('title', models.CharField(max_length=30)),
                ('description', models.TextField()),
                ('subtitle', models.CharField(max_length=30, null=True, blank=True)),
                ('image', models.FileField(null=True, upload_to=b'', blank=True)),
                ('drawing', models.FileField(null=True, upload_to=b'', blank=True)),
                ('assembly', models.ForeignKey(blank=True, to='toolkit.Assembly', null=True)),
                ('next_dir', models.ForeignKey(related_name='+', blank=True, to='toolkit.Direction', null=True)),
                ('system', models.ForeignKey(blank=True, to='toolkit.System', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='NaturalResource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.AlterField(
            model_name='component',
            name='c_type',
            field=models.CharField(max_length=30, choices=[(b'bearning', b'bearing'), (b'pole', b'pole'), (b'blade', b'blade')]),
        ),
    ]
