# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('toolkit', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='System',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('s_type', models.CharField(blank=True, max_length=30, null=True, choices=[(b'generator', b'generator'), (b'inverter', b'inverter'), (b'battery', b'battery'), (b'transformer', b'transformer'), (b'pv_module', b'pv_module')])),
                ('description', models.TextField()),
                ('image', models.ImageField(null=True, upload_to=b'/static/images', blank=True)),
                ('drawing', models.FileField(null=True, upload_to=b'/static/images/drawings', blank=True)),
                ('assembly', models.ManyToManyField(to='toolkit.Assembly', null=True, blank=True)),
            ],
        ),
    ]
