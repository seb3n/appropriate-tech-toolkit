# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('toolkit', '0005_adding_purpose_its_manager_its_relationships_to_previous_class_V0_1'),
    ]

    operations = [
        migrations.CreateModel(
            name='Method',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('method_adjective', models.CharField(max_length=30, choices=[(b'mechanical', b'mechanical'), (b'rotational', b'rotational'), (b'rotation', b'rotation'), (b'chemical', b'chemical'), (b'photoelectric', b'photoelectric')])),
                ('method_noun', models.CharField(max_length=30, choices=[(b'vibration', b'vibration'), (b'rotation', b'rotation'), (b'water', b'water'), (b'power', b'power'), (b'electricity', b'electricity'), (b'effect', b'effect'), (b'conductors', b'conductors')])),
                ('mode', models.DecimalField(default=None, null=True, max_digits=5, decimal_places=3, blank=True)),
                ('type_title', models.CharField(max_length=30)),
                ('description', models.TextField(default=None, null=True, blank=True)),
                ('amplitude', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
                ('angular_velocity', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
                ('damping_ratio', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
                ('frequency', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
                ('initial_force', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
                ('mass', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
                ('natural_frequency', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
                ('purpose', models.ManyToManyField(to='toolkit.Purpose', null=True, blank=True)),
                ('radius', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
                ('torque', models.ForeignKey(related_name='+', default=None, blank=True, to='toolkit.Quantity', null=True)),
            ],
        ),
    ]
