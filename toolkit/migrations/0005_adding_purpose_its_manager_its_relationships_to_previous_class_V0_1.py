# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('toolkit', '0004_adding_material_and_quantity_their_manager_their_relationships_to_previous_class_V0_1'),
    ]

    operations = [
        migrations.CreateModel(
            name='Purpose',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('purp_verb', models.CharField(max_length=20, choices=[(b'generate', b'generate'), (b'store', b'store'), (b'heat', b'heat'), (b'filter', b'filter'), (b'move', b'move'), (b'transfer', b'transfer'), (b'combine', b'combine')])),
                ('purp_noun', models.CharField(max_length=20, choices=[(b'vibration', b'vibration'), (b'rotation', b'rotation'), (b'water', b'water'), (b'power', b'power'), (b'electricity', b'electricity'), (b'effect', b'effect'), (b'conductors', b'conductors')])),
                ('component', models.ManyToManyField(to='toolkit.Component', null=True, blank=True)),
                ('system', models.ManyToManyField(to='toolkit.System', null=True, blank=True)),
            ],
        ),
    ]
