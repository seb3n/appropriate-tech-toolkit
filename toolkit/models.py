"""
Author: Benoit Sebastien, Mckinnley Workman

File Description: 
   
"""

from django.db import models
from django.core.exceptions import ValidationError
import logging
import os
from termcolor import colored

VERBS_LIST      = (     ('generate', 'generate'),
                        ('store', 'store'),
                        ('heat', 'heat'),
                        ('filter', 'filter'),
                        ('move', 'move'),
                        ('transfer', 'transfer'),
                        ('combine', 'combine'),
                        ('potential','potential'),
                        ('provide','provide')
                   )
            

NOUNS_LIST      = (     ('vibration', 'vibration'),
                        ('rotation', 'rotation'),
                        ('water', 'water'),
                        ('air','air'),
                        ('heat','heat'),
                        ('temperature','temperature'),
                        ('power', 'power'),
                        ('electricity', 'electricity'),
                        ('effect', 'effect'),
                        ('moisture','moisture'),
                        ('electromagnetic','electromagnetic'),
                        ('gravitational','gravitational'),
                        ('safety','safety'),
                        ('conduction','conduction'),
                        ('torque','torque'),
                        ('mechanical', 'mechanical'),
                        ('aerodynamic','aerodynamic'),
                        ('conductor', 'conductor') 
                  )

ADJECTIVES_LIST  = (    ('mechanical', 'mechanical'),
                        ('rotational', 'rotational'),
                        ('rotation', 'rotation'),
                        ('chemical', 'chemical'),
                        ('force','force'),
                        ('radiation','radiation'),
                        ('gradient','gradient'),
                        ('photoelectric', 'photoelectric'),
                        ('difference','difference'),
                        ('interuption','interuption'),
                        ('lift','lift'),
                        ('conduction','conduction')
                   )

SYS_TYPES       = (     ('generator', 'generator'),
                        ('inverter', 'inverter'),
                        ('battery', 'battery'),
                        ('transformer', 'transformer'),
                        ('pv_module', 'pv_module')
                   )
    
COMP_TYPES       = (    ('bearning', 'bearing'),
                        ('pole', 'pole'),
                        ('blade','blade')
                   )
                   
BASE_DIR = os.path.dirname(__file__)
IMAGE_DIR = os.path.join(BASE_DIR, '/static/images')
DRAWING_DIR = os.path.join(BASE_DIR, '/static/images/drawings')

class NaturalResourceManager(models.Manager):
    
    def create_natural_resource(self, title, description, icon_title):
        """
        create an empty Natural Resource object and saving it.
        
        Ex: wind = NaturalResource.objects.create_natural_resource()
        """
        nr = self.create()
        
        try:
            #add required parameters
            nr.title = title
            nr.description = description
            nr.icon_title = icon_title
            
            nr.save()
        except:
            raise ValueError("Could not add all required fields. Check requirements")
        return nr
        
class NaturalResource(models.Model):
    """
    The Natural Resource model ...
    
    """
    title       = models.CharField(max_length=30, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    icon_title  = models.CharField(max_length=20, null=True, blank=True)
    
    # A Natural resource should be able to have many purposes
    purpose     = models.ForeignKey('Purpose', null=True, blank=True, default=None)
    
    objects = NaturalResourceManager()
    
    def set_purpose_method(self, purp, meth):
        """
        Set the purpose of the natural resource and the method it uses to 
        accomplish it
        
        Params:
        purp: must be made up of two words (Ex. word1_word2)
        meth: must be made up of two words (Ex. word1_word2)
        
        Ex. r.set_purpose_method(self, 'provide_moisture', 'gravitational_force'):
        """
        try:
            p = purp.split('_')
            m = meth.split('_')
            
            # Create the method and assign it self
            my_method = Method.objects.create_method(method_noun=m[0], method_adjective=m[1], natural_resource=self)
            my_method.save()
            
            # Create the purpose and assign it self
            q = Purpose.objects.create_purpose(purp_verb=p[0], purp_noun=[1], method=my_method)
            q.save()
            
            return self
        except:
            return False
        
    def set_io_quantity(self, title, io, quant_list=None, unit=None, coord_sys=None):
        """
        """
        try:
            
            # Create a quantity
            nr_q = Quantity.objects.create_quantity(usr_val_list = quant_list or None,
                                                    usr_unit     = unit or None,
                                                    short_title  = title,
                                                    usr_coor     = coord_sys or None,
                                                    io_type      = io
                                                    )
            
            # Add it to the current NR
            self.quantity_set.add(nr_q)
            return self
            
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
        
    def __unicode__(self):
        return self.title
    
class SystemManager(models.Manager):
    """
    The System Manager handles processing multiple rows and creation, deletion, 
    etc of systems
    """
    
    def create_system(  self, s_type, description, icon_title=None, image= None, drawing=None):
        """
        Create an empty system object and saving it
        
        Ex: generator_system = System.objects.create_system()
        Ex: inverter_system =System.objects.create_system()
        """
        s = self.create()
        
        try:
            # Add required parameters
            s.s_type      = s_type
            s.description = description
            
            # Add optional parameters
            s.image       = image or None
            s.drawing     = drawing or None
            s.icon_title  = icon_title or None 
            
            s.save()
        except:
            raise ValueError("Could not add all required fields. Check requirements")
        return s
        
    def get_io_quantities(self, s, i_or_o ,display_q_type=False, display_required=False):
        """
        Get system input or output quantity
        
        Params:
        q_type: Allows you to return a string of the input type rather than the objects themselves
        required: Allows user to return only required inputs
        i_or_o: 'i' if you want inputs, 'o' if you want outputs
        """
        try:
            # Get a list of inputs or outputs
            if i_or_o == 'i':
                quantity_list = s.quantity_set.filter(io_type='i')
            elif i_or_o == 'o':
                quantity_list = s.quantity_set.filter(io_type='o')
            else:
                raise ValueError('i or o are the only options for i_or_o paramter')
            
            l=[]
            # Replace the list with appropriate fields based on parameters given
            if display_q_type and display_required:
                # Return required quantity types only
                for i in range(len(quantity_list)):
                    if quantity_list[i].io_is_required():
                        l.append(quantity_list[i].get_qtype())

            elif display_q_type and not display_required:
                # Return all quantity TYPES
                for i in range(len(quantity_list)):
                    l.append(quantity_list[i].get_qtype())
            elif not display_q_type and display_required:
                # Return quantity OBJECTS that are required
                for i in range(len(quantity_list)):
                    if quantity_list[i].io_is_required():
                        l.append(quantity_list[i])
            else:
                return l
            
            return l
        
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
            
    def required_io_fields_have_valid_links(self, s):
        """
        Return True if all required inputs and outputs have valid links; False otherwise
        """
        
        required_field = self.get_io_quantities(s, required=True)
        for i in range(len(required_fields)):
            if not required_fields[i].is_a_valid_link():
                return False
        return True
    
    def get_links(self, s):
        """
        Return a list of the links associated with the sytem
        """
        l=[]
        
        for system_quantity in s.quantity_set:
            l.append(Quantity.objects.get_links(assemb))
        
        return l
    
    def list_system_materials(self, s):
        """
        Returns a list of the material objects that belong to a system
        """
        return s.material_set.all()
        
    def list_components(self, usr_system):
        """
        Return a list of components that belong to the system that the user enters
        """
        return usr_system.component_set.all()
    
class System(models.Model):
    """
    System Definition: A System is a single unit that has a single function/purpose
    
    Args:
        type: describes the general function of system based on a list of SYS_TYPES
        description: Short text, describing the system, its functioning and its purpose
        image: file upload of the system image
        icon: file upload of system icon
        drawing: file upload of system drawing (could be any CAD file, etc)
    """

    s_type           = models.CharField(max_length=30, choices=SYS_TYPES, null=True, blank=True)
    icon_title       = models.CharField(max_length=15, null=True, blank=True)
    description      = models.TextField()
    image            = models.ImageField(null=True, blank=True, upload_to=IMAGE_DIR)
    drawing          = models.FileField(null=True, blank=True, upload_to=DRAWING_DIR)

    # Parameters for directing the assembly
    assembly         = models.ManyToManyField('Assembly', null=True, blank=True)
    
    objects          = SystemManager()
    
    def add_component_to_system(self, comp):
        """
        Add a component to a system
        """
        return self.component_set.add(comp)
    
    def is_valid(self, s):
        """
        Returns True or False based on if the system is valid or not. 
        
        Validity Requirements:
            type: must be not None
            description: must be not None
            purpose: must be not None
            method: must be not None
        """
       
       # Check validity of all components that are part of the system
        components = self.component_set.all()
        for comp in components:
            if not comp.is_valid(): return False
       
        # Make sure the required variables have values
        if (self.s_type or self.description) == None: 
            return False
        
        # Make sure it has a valid Purpose and Method
        try:
            purp = s.purpose_set.all()
            if purp == None:
                return False
        except:
            raise ValueError(colored("No purpose is defined for this system","red"))
        if purp != None:
            if not purp.is_valid(): 
                return False
        return True
    
    def display_system(self, with_io=False, with_links=False):
        """
        Displays a visually appealing view of system object
        
        Params:
        with_io: diplays system with all of its IO components
        with_links: displays system with all of its links
        """
        
        print "System     : ", self.s_type
        print "Purpose and Method: "
        Purpose.objects.display_purp_meth(self)
        print "Description: ", self.description
        if with_io:
            print "\t System Input Quantities: "
            Quantity.objects.display_quantities(self, i_or_o='i')
            print "\t System Output Quantities: "
            Quantity.objects.display_quantities(self, i_or_o='o')
        
    def set_purpose_method(self, purp, meth):
        """
        Set the purpose of the natural resource and the method it uses to 
        accomplish it
        
        Params:
        purp: must be made up of two words (Ex. word1_word2)
        meth: must be made up of two words (Ex. word1_word2)
        
        Ex. r.set_purpose_method(self, 'provide_moisture', 'gravitational_force'):
        """
        try:
            p = purp.split('_')
            m = meth.split('_')
            
            # Create the method and assign it self
            if (m[0] or p[0] or m[1] or p[1]) == '': raise ValueError("Must be word1_word2")
            my_method = Method.objects.create_method(method_noun=m[0], method_adjective=m[1], system=self)
            
            # Create the purpose and assign it self
            q = Purpose.objects.create_purpose(purp_verb=p[0], purp_noun=p[1])
            q.method_set.add(my_method)
            self.purpose_set.add(q)
            
            return self
        except:
            return False
    
    def set_io_quantity(self, title, io, quant_list=None, unit=None, coord_sys=None):
        """
        """
        try:
            
            # Create a quantity
            sys_q = Quantity.objects.create_quantity(usr_val_list = quant_list or None,
                                                    usr_unit     = unit or None,
                                                    short_title  = title,
                                                    usr_coor     = coord_sys or None,
                                                    io_type      = io
                                                    )
            
            # Add it to the current sys
            self.quantity_set.add(sys_q)
            return self
            
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
   
    def set_direction(self,  title_list, categ, description_list, image_list=None, drawing_list=None, subtitle_list=None):
        """
        Set a list of directions of a certain type (ie. maintenance directions)
        """
        try:
            dir_obj_list = [ Direction()] * len(title_list)
            for i in range(len(title_list)):
                
                dir_obj_list[i].category    = categ
                dir_obj_list[i].title       = title_list[i] or None
                dir_obj_list[i].description = description_list[i] or None
                
                if image_list   != None: dir_obj_list[i].image    = image_list[i]
                if drawing_list != None: dir_obj_list[i].drawing  = drawing_list[i]
                if subtitle_list!= None: dir_obj_list[i].subtitle = subtitle_list[i]
                
                dir_obj_list[i].save()
                
                if i < len(title_list)-2:
                    dir_obj_list[i].next_dir = dir_obj_list[i+1]
                    
                dir_obj_list[i].save()
                
                self.direction_set.add(dir_obj_list[i])
            return self
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
        
    def __unicode__(self):
        return self.s_type

class AssemblyManager(models.Manager):
    """
    The Assembly Manager deals with handeling multiple assembly instances
    """
    
    def create_assembly(self, title=None, description=None):
        """
        Creates an empty assembly and returns the assembly object
        """
        try:
            a = self.create()
            a.title = title or None
            a.description = description or None
            a.save()
            return a
        except Exception as e:
            print colored("could not create assembly","red")
            logging.exception('Exception: ' + str(e))
            
    def list_systems(self, usr_assembly):
        """
        Return a list of systems that belong to the assembly that the user enters
        """
        return usr_assembly.system_set.all()
    
    def list_components(self, usr_assembly):
        """
        Return a list of components that belong to the assembly that the user enters
        """
        return usr_assembly.component_set.all()    
    
class Assembly(models.Model):
    """
    Assembly Definition: An Assembly contains many systems and components
    """

    title         = models.CharField(max_length=30, null=True, blank=True)
    description   = models.TextField(null=True, blank=True)
    valid_assembly = models.BooleanField(default=False)
    
    objects = AssemblyManager()
    
    def add_system_to_assembly(self, sys):
        """
        Add a system to an assembly
        """
        return self.system_set.add(sys)
        
    def add_component_to_assembly(self, comp):
        """
        Add a component to an assembly
        """
        return self.component_set.add(comp)    
    
    def is_a_valid_assembly(self):
        """
        Returns True is assembly meets validity requirements
        
        Validity Requirements:
        - all systems are valid (sys.is_valid() automatically checks validity of its quantities, sub-componentsetc)
        - all components are valid (comp.is_valid() automatically checks validity of its quantities, etc)
        - all links are valid
        """
        
        # Check validity of all systems that are part of the assembly
        systems = self.system_set.all()
        for sys in systems:
            if not sys.is_valid(): return False
            
        # Check validity of all components that are part of the assembly
        components = self.component_set.all()
        for comp in components:
            if not comp.is_valid(): return False
        
        # Check validity of all assembly links
        link_tuples = Quantity.objects.get_links(self)
        for link in link_tuples:
            if not link.is_a_valid_link(): return False
        
        return True
    
    def display_assembly(self, show_complete=True):
        """
        Display the assembly as it currently is
        
        Params:
        show_complete: is a boolean that allows a completly detailed display.
        
        Ex. Assembly.objects.display_assembly(myAssembly)
        """
        
        # Get a list of systems and components
        sys_list = Assembly.objects.list_systems(self)
        comp_list = Assembly.objects.list_components(self)
        
        if len(sys_list) == 0:
            print "No Systems to display for this assembly"
        else:
            # Iterate through the systems
            for sys in sys_list:
                sys.display_system(with_io=show_complete, with_links=show_complete)
        
        if len(comp_list) == 0:
            print "No Components to display for this assembly"
        else:
            # Iterate through the components
            for comp in comp_list:
                comp.display_component(with_io=show_complete, with_links=show_complete)
    
    def __unicode__(self):
        return self.title
    
class QuantityManager(models.Manager):
    """
    The Quantity Manager controls the creation and representation of quantities
    """

    def create_quantity(self, usr_val_list, usr_unit, short_title=None, io_type=None, usr_coor=None):
        """
        Create a Quantity
        
        For Vectors you must give a coord system p, c, or r for polar, cylindrical
        or rectangular, respectively.
        
        Rect must be entered in order x, y, z
        Polar must be entered in order r, theta, phi
        Cylindrical must be entered in order r, s, phi
        
        Ex. create_quantity([ (2,3),(3,5),(4.2, 4.3) ],'N', 'r')
        Ex. create_quantity([ (2),(3),(4.2) ],'m', 'p')
        Ex. create_quantity([ (2),(3),(4.2) ],'m', 'p')
        Ex. create_quantity([ (652176) ], 's')
        """
        
        q = self.create()
        
        # Add optional params
        q.io_type = io_type or None
        q.short_title = short_title or None
        
        #There may be a faster way of searching a tuple...look into this
        if [x for x, v in enumerate(q.SCALAR_CHOICES) if v[1][1] == usr_unit] != []:
            q.scalar = True
            q.vector = False
            q.coord_type = None
            q.scalar_unit = usr_unit
        elif [x for x, v in enumerate(q.VECTOR_CHOICES) if v[1][1] == usr_unit] != []:
            q.vector = True
            q.scalar = False
            q.vector_unit = usr_unit
            if [x for x, v in enumerate(q.TYPE_CHOICES) if v[0] == usr_coor] == []:
                raise ValueError("You must enter a coorinate system (r, p, or c)")
            else:
                q.coord_type = q.TYPE_CHOICES[[x for x, v in enumerate(q.TYPE_CHOICES) if v[0] == usr_coor][0]][0]
        else:
            raise ValueError("Unit not in database")
        if len(usr_val_list) == 1:
            if q.scalar == False:
                raise ValueError("Scalars have 1 value Vectors have 3")
            else:
                if len(usr_val_list[0]) == 1:
                    q.val_A_max = q.val_A_min  = usr_val_list[0]
                else:
                    q.val_A_max  = usr_val_list[0][1]
                    q.val_A_min  = usr_val_list[0][0]
                q.val_B_max = q.val_B_min  = None
                q.val_C_max = q.val_C_min  = None
        elif len(usr_val_list) == 2:
            raise ValueError("Scalars have 1 value Vectors have 3")
        elif len(usr_val_list) == 3:
            if q.vector == False:
                raise ValueError("Scalars have 1 value Vectors have 3")
            else:
                if len(usr_val_list[0]) == 1:
                    q.val_A_max = q.val_A_min = usr_val_list[0]
                    q.val_B_max = q.val_B_min = usr_val_list[1]
                    q.val_C_max = q.val_C_min = usr_val_list[2]
                elif len(usr_val_list[0]) == 2:
                    q.val_A_min = usr_val_list[0][0]
                    q.val_A_max = usr_val_list[0][1]
                    q.val_B_min = usr_val_list[1][0]
                    q.val_B_max = usr_val_list[1][1]
                    q.val_C_min = usr_val_list[2][0]
                    q.val_C_max = usr_val_list[2][1]
                else:
                    raise ValueError("Check the examples.")
        elif len(usr_val_list) > 3:
            raise ValueError("More than three values are not allowed")
        q.save()
        return q
    
    def is_a_valid_quantity(self, usr_quantity):
        """
        Return True if the quantity meets validity requirements
        
        Validity Requirements:
            - vector XOR scalar
            - vector_unit XOR scalar_unit that matches the type
            - coordinate system must be specified
        """
        
        # Check that if its a vector, the vector unit is filled in
        if not (self.vector and (self.vector_unit != None)):
            return False
        # Check that if its a scalar, the scalar unit is filled in
        if not (self.scalar and (self.scalar_unit != None)):
            return False
            
        if self.coord_type == None:
            return False

        return True
    
    def is_a_valid_link(self, link_tuple):
        """
        Return True if the quantity meets validity requirements for a link
        
        Validity Requirements:
            - link_tip parameter must not be None
            - input must not go to output (if link_root type is input, then link_tip must not be output)
            - output must go to an input (if link_root type is an output, then link must be an input)
            - if input is required, it must be linked to an output
        """

        # Return False if link_tip is None (ie. not set to a Quantity object) and the link_tip is required
        if (link_tuple[1].link_tip == None) and link_tuple[1].io_is_required():
            return False
            
        # A link_root must be an output
        if link_tuple[0].io_type != 'o':
            return False
        
        # A link_tip must be an input
        if link_tuple[1].io_type != 'i':
            return False
        
        return True
        
    def get_links(self, usr_assembly):
        """
        Return a list of linked quantity tuples that belong to the assembly
        
        Params:
        usr_assembly: the assembly object to get links for
        """
        l=[]
        
        # Get a list of systems from the assembly
        system_list    = usr_assembly.system_set.all()
        component_list = usr_assembly.component_set.all()
        
        for sys in system_list:
            for link in sys.quantity_set.filter(link_root=True):
                # Return a list of quantity objectts links like [(q1, q5), (q4, q2)]
                l.append((link, link.link_tip))
        
        for comp in component_list:
            for link in comp.quantity_set.filter(link_root=True):
                # Return a list of quantity objectts links like [(q1, q5), (q4, q2)]
                l.append((link, link.link_tip))
       
        return l
    
    def display_quantities(self, sys=None, comp=None, nr=None, i_or_o=None):
        """
        Displays quantities
        
        @param i_or_o 
        @param comp
        @param sys
        """
        
        item = sys or comp or nr
        
        for q in item.quantity_set.filter(io_type=i_or_o):
            print "\n \t Title: ",  q.short_title
            print "\t Unit: ", q.scalar_unit or q.vector_unit
            print "\t Range A: (", q.val_A_min, ",", q.val_A_max,")"
            print "\t Range B: (", q.val_B_min, ",", q.val_B_max,")"
            print "\t Range C: (", q.val_C_min, ",", q.val_C_max,")"

class Quantity(models.Model):
    """
    A measureable quantity that goes into or comes out of something
    """
    
    SCALAR_CHOICES = (
                            ('power'                ,  (   ('watt', 'W') )),
                            ('irradiance'           ,  (   ('irradiance', 'W/m2') )),
                            ('work'                 ,  (   ('joule', 'J') )),
                            ('length'               ,  (   ('meter', 'm') )),
                            ('mass'                 ,  (   ('kilogram', 'kg') )),
                            ('volume'               ,  (   ('liter', 'l') )),
                            ('electric charge'      ,  (   ('coulomb', 'C') )),
                            ('electric potential'   ,  (   ('voltage', 'v') )),
                            ('resistance'           ,  (   ('ohm', u'U+2126') )),
                            ('electric conductance' ,  (   ('siemens', 'S') )),
                            ('capacitance'          ,  (   ('Farad', 'F') )),
                            ('inductance'           ,  (   ('Henry', 'H') )),
                            ('frequency'            ,  (   ('hertz', 'Hz') )),
                            ('time'                 ,  (   ('second', 's') )),
                            ('luminous intensity'   ,  (   ('candela', 'cd') )),
                            ('illuminance'          ,  (   ('lux', 'lx') )),
                            ('luminous flux'        ,  (   ('lumen', 'lm') )),
                            ('pressure'             ,  (   ('pascal', 'Pa') )),
                            ('magnetic flux'        ,  (   ('weber', 'Wb') )),
                            ('magnetic flux density',  (   ('tesla', 'T') )),
                            ('activity'             ,  (   ('bequerel', 'Bq') )),
                            ('amount of substance'  ,  (   ('mole', 'mol') )),
                            ('absorbed dose'        ,  (   ('gray', 'Gy') )),
                            ('dose equivalent'      ,  (   ('sievert', 'Sv') )),
                            ('catalyctic activity'  ,  (   ('katal', 'kat')) ),
                            ('density'              ,  (   ('rho', 'kg/m3')) ),
                            ('persentage'           ,  (   ('percent', '%') )),
                            ('temperature'          ,  (   ('kelvin', 'K') )),
                            ('energy'               ,  (   ('kilowatt-hour', 'kWh') )),
                            ('flow_rate'            ,  (   ('meter cubed per second', 'm3/s') ))
                     )
    
    VECTOR_CHOICES = (   
                             ('force'               ,  (   ('newton', 'N') )),
                             ('length'              ,  (   ('meter' , 'm'))),
                             ('velocity'            ,  (   ('mps', 'm/s') )),
                             ('current'             ,  (   ('amperage', 'amps') )),
                             ('torque'              ,  (   ('newton meter', 'Nm') )),
                             ('angular velocity'    ,  (   ('radian per second', 'rad/s') )),
                             ('acceleration'        ,  (   ('meter per second squared', 'm/s2') ))
                     )
    
    TYPE_CHOICES   = (        
                            ('r','rectangular coord system'), 
                            ('p','polar coord system'),
                            ('c','cylindrical coord system'),
                     )

    IO_CHOICES      =   (
                        ('','None'),
                        ('i','input'),
                        ('o','output')
                        )
    
    short_title      = models.CharField(max_length=15, null=True, blank=True)
    vector           = models.BooleanField(default=False)
    scalar           = models.BooleanField(default=False)
    scalar_unit      = models.CharField(max_length=30, choices=SCALAR_CHOICES, blank=True)
    vector_unit      = models.CharField(max_length=30, choices=VECTOR_CHOICES, blank=True)
    coord_type       = models.CharField(max_length=10, null=True, choices=TYPE_CHOICES, default=TYPE_CHOICES[0])
    val_A_max        = models.DecimalField(max_digits=15, decimal_places=5, null=True, default=None, blank=True)
    val_A_min        = models.DecimalField(max_digits=15, decimal_places=5, null=True, default=None, blank=True)
    val_B_max        = models.DecimalField(max_digits=15, decimal_places=5, null=True, default=None, blank=True)
    val_B_min        = models.DecimalField(max_digits=15, decimal_places=5, null=True, default=None, blank=True)
    val_C_max        = models.DecimalField(max_digits=15, decimal_places=5, null=True, default=None, blank=True)
    val_C_min        = models.DecimalField(max_digits=15, decimal_places=5, null=True, default=None, blank=True)
    
    # Natural Resource
    nat_resource       = models.ManyToManyField('NaturalResource', null=True, blank=True)
    
    # Quantity may belong to a Material
    material           = models.ManyToManyField('Material', null=True, blank=True)

    # Quantity may belong to a System or a Component
    system             = models.ForeignKey('System', null=True, blank=True)
    component          = models.ForeignKey('Component', null=True, blank=True)
    io_type            = models.CharField(max_length=5, choices=IO_CHOICES, null=True, blank=True, default=IO_CHOICES[0][0])
    required           = models.NullBooleanField(default=False, null=True, blank=True)
    
    # Is the quantity an input or output?
    link_root          = models.NullBooleanField(default=False, null=True, blank=True)
    link_tip           = models.ForeignKey('self', null=True, blank=True)
    
    objects            = QuantityManager()
    
    def is_valid_quantity(self):
        """
        Return true if quantity meets validity requirements
        
        Validity Requirements:
        """
        return True
        
    def io_is_required(self):
        """
        Return True if the input or output is required
        """
        return required
    
    def set_required(self, requirement):
        """
        Set required field for the quantity
        """
        self.required = requirement
        return self.save()
    
    def get_qtype(self):
        """
        Return the type of quantity
        """
        if self.vector:
            return self.vector_unit
        elif self.scalar:
            return self.scalar_unit
        else:
            raise ValueError("Units are not set properly in Quantity object")
    
    def assign_io_to_system(self, sys):
        """
        Assign a system to a quantity and assign the quantity as an input
        or output of the system
        
        Ex. assign_io_to_system(generator_system)
        """
        if self.is_valid_quantity():
            self.system = sys
            self.save()
        else:
            raise ValueError("Your IO input is not right. See examples.")
    
    def assign_io_to_component(self, comp):
        """
        Assign a component to a quantity and assign the quantity as an input
        or output of the component
        
        Ex. assign_io_to_component(blade)
        """
        if self.is_a_valid_quantity():
            self.component = comp
            self.save()
        else:
            raise ValueError("Your IO input is not right. See examples.")
    
    def __unicode__(self):
        return (self.short_title, self.get_qtype())
    
class ComponentManager(models.Manager):
    """
    The Component Manager handles processing multiple rows and creation, deletion, 
    etc of components
    """
    def create_component(  self, c_type, description, icon_title=None, image= None, drawing=None):
        """
        Create an empty system object and saving it
        
        Ex: generator_system = System.objects.create_system()
        Ex: inverter_system =System.objects.create_system()
        """
        c = self.create()
        
        try:
            # Add required parameters
            c.c_type      = c_type
            c.description = description
            
            # Add optional parameters
            c.image       = image or None
            c.drawing     = drawing or None
            
            c.save()
        except:
            raise ValueError("Could not add all required fields. Check requirements")
        return c

    def get_io_quantities(self, c, i_or_o ,display_q_type=False, display_required=False):
        """
        Get component input or output quantity
        
        Params:
        q_type: Allows you to return a string of the input type rather than the objects themselves
        required: Allows user to return only required inputs
        i_or_o: 'i' if you want inputs, 'o' if you want outputs
        """
        try:
            # Get a list of inputs or outputs
            if i_or_o == 'i':
                quantity_list = c.quantity_set.filter(io_type='i')
            elif i_or_o == 'o':
                quantity_list = c.quantity_set.filter(io_type='o')
            else:
                raise ValueError('i or o are the only options for i_or_o paramter')
            
            l=[]
            # Replace the list with appropriate fields based on parameters given
            if display_q_type and display_required:
                # Return required quantity types only
                for i in range(len(quantity_list)):
                    if quantity_list[i].io_is_required():
                        l.append(quantity_list[i].get_qtype())

            elif display_q_type and not display_required:
                # Return all quantity TYPES
                for i in range(len(quantity_list)):
                    l.append(quantity_list[i].get_qtype())
            elif not display_q_type and display_required:
                # Return quantity OBJECTS that are required
                for i in range(len(quantity_list)):
                    if quantity_list[i].io_is_required():
                        l.append(quantity_list[i])
            else:
                return l
            
            return l
        
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
            
    def required_io_fields_have_valid_links(self, c):
        """
        Return True if all required inputs and outputs have valid links; False otherwise
        """
        
        required_field = self.get_io_quantities(c, required=True)
        for i in range(len(required_fields)):
            if not required_fields[i].is_a_valid_link():
                return False
        return True
    
    def get_links(self, c):
        """
        Return a list of the links associated with the component
        """
        l=[]
        
        for component_quantity in c.quantity_set:
            l.append(Quantity.objects.get_links(assemb))
        
        return l
    
    def list_component_materials(self, c):
        """
        Returns a list of the material objects that belong to a component
        """
        return c.material_set.all()
    
class Component(models.Model):
    """
    Component Definition: A Component is a part of an assembly or system
    """
    
    c_type        = models.CharField(max_length=30, choices=COMP_TYPES)
    icon_title    = models.CharField(max_length=15, null=True, blank=True)
    description   = models.TextField()
    drawings      = models.FileField(upload_to=None, max_length=100)
    system        = models.ManyToManyField('System')
    assembly      = models.ForeignKey('Assembly', null=True, blank=True) #component andsystem have != relationship with assembly
    
    objects       = ComponentManager()
    
    def display_component(self, with_io=False, with_links=False):
        """
        Displays a visually appealing view of component object
        
        Params:
        with_io: diplays component with all of its IO quantities
        with_links: displays component with all of its links
        """
        
        print "Component: ", self.c_type
        print "Purpose and Method: "
        Purpose.objects.display_purp_meth(self)
        print "Description: ", self.description
        if with_io:
            print "Component Input Quantities: "
            Quantity.objects.display_quantities(self, i_or_o='i')
            print "Component Output Quantities: " 
            Quantity.objects.display_quantities(self, i_or_o='o')
    
    def set_purpose_method(self, purp, meth):
        """
        Set the purpose of the natural resource and the method it uses to 
        accomplish it
        
        Params:
        purp: must be made up of two words (Ex. word1_word2)
        meth: must be made up of two words (Ex. word1_word2)
        
        Ex. r.set_purpose_method(self, 'provide_moisture', 'gravitational_force'):
        """
        p = purp.split('_')
        m = meth.split('_')
        
        if (m[0] or p[0] or m[1] or p[1]) == '': raise ValueError("Must be word1_word2")
        
        # Create the method and assign it self
        my_method = Method.objects.create_method(method_noun=m[0], method_adjective=m[1], component=self)
        
        # Create the purpose and assign it self
        q = Purpose.objects.create_purpose(purp_verb=p[0], purp_noun=p[1])
        q.method_set.add(my_method)
        
        self.purpose_set.add(q)
        
        
        return self
    
    def set_io_quantity(self, title, io, quant_list=None, unit=None, coord_sys=None):
        """
        """
        try:
            
            # Create a quantity
            comp_q = Quantity.objects.create_quantity(usr_val_list = quant_list or None,
                                                    usr_unit     = unit or None,
                                                    short_title  = title,
                                                    usr_coor     = coord_sys or None,
                                                    io_type      = io
                                                    )
            
            # Add it to the current comp
            self.quantity_set.add(comp_q)
            return self
            
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
    
    def is_valid(self, c):
        """
        Returns True or False based on if the component is valid or not. 
        
        Validity Requirements:
            type: must be not None
            description: must be not None
            purpose: must be not None
            method: must be not None
        """
        # Make sure the required variables have values
        if (self.c_type or self.description) == None: 
            return False
        
        # Make sure it has a valid Purpose and Method
        try:
            purp = c.purpose_set.all()
            if purp == None:
                return False
        except:
            raise ValueError(colored("No purpose is defined for this component","red"))
        if purp != None:
            if not purp.is_valid(): 
                return False
        return True
        
    def set_direction(self,  title_list, categ, description_list, image_list=None, drawing_list=None, subtitle_list=None):
        """
        Set a list of directions of a certain type (ie. maintenance directions)
        """
        try:
            dir_obj_list = [ Direction()] * len(title_list)
            for i in range(len(title_list)):
                
                dir_obj_list[i].category    = categ
                dir_obj_list[i].title       = title_list[i] or None
                dir_obj_list[i].description = description_list[i] or None
                
                if image_list   != None: dir_obj_list[i].image    = image_list[i]
                if drawing_list != None: dir_obj_list[i].drawing  = drawing_list[i]
                if subtitle_list!= None: dir_obj_list[i].subtitle = subtitle_list[i]
                
                dir_obj_list[i].save()
                
                if i < len(title_list)-2:
                    dir_obj_list[i].next_dir = dir_obj_list[i+1]
                    
                dir_obj_list[i].save()
                
                self.direction_set.add(dir_obj_list[i])
            return self
        except Exception as e:
            logging.exception('Exception: ' + str(e))
            return False
    
    def __unicode__(self):
        return self.c_type

class MaterialManager(models.Manager):
    """
    Ex. create_material(common_name='aluminum', color='silver', appearance='shiny and bla', 
                        texture='smooth', odor='odorless', toxicity='0', uses='fastening', 
                        category='metal', )
    """
    
    def create_material(self, common_name, toxicity, density, category, color=None, \
                        appearance=None, texture=None, odor=None, uses=None, \
                        sustainability_rating=None, sustainability_notes=None, \
                        image=None, scientific_name=None, distribution=None, \
                        tree_height=None, trunk_diameter=None, average_dried_weight=None, \
                        specific_gravity=None, janka_hardness=None, modulus_of_rupture=None, \
                        elastic_modulus=None, crushing_strength=None, shrinkage=None, endgrain=None, \
                        rot_resistance=None, workability=None, molecular_weight=None, CAS_number=None, \
                        crystal_structure=None, modulus_of_elasticity=None, ferrous=None, \
                        electric_conductivity=None, ductility=None, melting_point=None, hardness = None, \
                        thermal_condutivity = None, refractive_index = None, ionization_energy = None):
        """
        Requires field depends on application ofthe material. For instance,
        the required properties of aluminium for a mirror would be different then for a structure.
        So just like we have category, we need application of material.
        Not needed as required field:
                                     Odor, texture, apearance,
        Needed as required field:
                                 Name, youngs modulus, yield strength, yield stress,
                                 electric_conductivity, thermal_condutivity, density,
                                 corosion resistance, toxicity,category, application/use,
                                 
        Needed as optional field:
                                 Melting point, phase at room temperature, optical properties,
                                 hardness, coef of thermal expansion, coef of friction, 
                                 thermal diffusion, plasticity, percent composition...
                                 ref: https://en.wikipedia.org/wiki/List_of_materials_properties
                                 
        """
        m = self.create()
       
        try: 
            #Assign Required Variables
            m.common_name             = common_name
            m.toxicity                = toxicity
            m.color                   = color
            m.density                 = density
            m.category                = category
        
            #Assign Optional Variables
            m.sustainability_rating   = sustainability_rating or None
            m.sustainability_notes    = sustainability_notes or None
            m.image                   = image or None
            m.appearance              = appearance or None
            m.texture                 = texture or None
            m.odor                    = odor or None #not necessary.
            m.uses                    = uses or None
            
           
            
            if( m.common_name or m.color or m.appearance or m.toxicity or \
                m.density or m.category) == None:
                raise TypeError(colored("Required variables for all materials must be included...","red"))
        except Exception as e:
            print colored("Couldn't do it :P","red")
            logging.exception('Exception: ' + str(e)) # import logging
            return False
        
        #Assign Category Specific Variables
        if m.category == 'wood':
            try:
                m.scientific_name      = scientific_name
                m.distribution         = distribution
                m.tree_height          = tree_height
                m.trunk_diameter       = trunk_diameter
                m.average_dried_weight = average_dried_weight
                m.specific_gravity     = specific_gravity
                m.janka_hardness       = janka_hardness
                m.modulus_of_rupture   = modulus_of_rupture
                m.elastic_modulus      = elastic_modulus
                m.crushing_strength    = crushing_strength
                m.shrinkage            = shrinkage
                m.endgrain             = endgrain
                m.rot_resistance       = rot_resistance
                m.workability          = workability
                
                if(m.scientific_name or m.distribution \
                or tree_height or trunk_diameter or average_dried_weight \
                or specific_gravity or janka_hardness or modulus_of_rupture \
                or elastic_modulus or crushing_strength or shrinkage or \
                endgrain or rot_resistance or workability) == None:
                    raise TypeError("All variables in list_material_properties() must be included")
            except Exception as e:
                print "Could not be created ..."
                logging.exception('Exception: ' + str(e)) 
                
                
        if m.category == 'metal':
            print "adding metal"
            try:
                m.molecular_weight      = molecular_weight
                m.CAS_number            = CAS_number
                m.crystal_structure     = crystal_structure
                m.modulus_of_elasticity = modulus_of_elasticity
                m.ferrous               = ferrous
                m.electric_conductivity = electric_conductivity
                m.ductility             = ductility
                m.melting_point         = melting_point
                m.hardness              = hardness
                m.thermal_condutivity   = thermal_condutivity
                m.refractive_index      = refractive_index
                m.ionization_energy     = ionization_energy
                
               
                if(m.molecular_weight or m.CAS_number or m.crystal_structure or \
                m.modulus_of_elasticity or m.ferrous or m.electric_conductivity or \
                m.ductility or m.melting_point or m.hardness or m.thermal_condutivity \
                or m.refractive_index or m.ionization_energy ) == None:
                    raise TypeError( colored("All variables in list_material_properties() must be included","red"))
            except Exception as e:
                print colored("Could not be created ...","red")
                logging.exception('Exception: ' + str(e)) 
                
                
        m.save()
        return m
        
    def list_material_properties(self, categ):
        """
        Get the properties associated with a specific category of material
        
        Ex: list_material_properties('wood')
            >> [common_name, color, appearance, texture, odor, etc]
        """
        print   "Required: common_name, color, appearance, texture, odor, toxicity, uses, density, image, \
                sustainability_rating, sustainability_notes, category"
                
        print   "Optional: image, "
        
        if categ == 'wood':
            print   "Wood Specific Required Parameters: ask me to finish this"
        elif categ == 'metal':
            print   "Metal Specific Required Parameters: ask me to finish this"
        pass
        
class Material(models.Model):
    """
    Wood Database Source: http://www.wood-database.com/
    Metals Database Source:
    """
    
    TOCICITY_CHOICES = ( ('1', 'I'), ('2','II'), ('3','III'), ('4', 'IV'))
    
    MATERIALS = (   ('wood','wood'),
                    ('composite','composite'),
                    ('metal','metal'),
                    ('polymer','polymer'),
                    ('semiconductor','semiconductor'),
                    ('metamaterial','metamaterial'),
                    ('ceramic','ceramic'),
                    ('concrete','concrete'),
                    ('glass','glass'),
                    ('electronic/optical','electronic/optical')
                )
    
    # The component that the particular instance of Material belongs to
    component = models.ManyToManyField('Component', null=True, blank=True)
    
  # quantity  = models.ManyToManyField('Quantity', null=True, blank=True)
    
    # Properties of ALL Materials #
    common_name           = models.CharField(max_length=30)
    color                 = models.TextField(null=True, blank=True, default=None)
    appearance            = models.TextField(null=True, blank=True, default=None)
    texture               = models.TextField(null=True, blank=True, default=None)
    odor                  = models.TextField(null=True, blank=True, default=None)
    toxicity              = models.IntegerField(choices=TOCICITY_CHOICES, null=True, blank=True)
    uses                  = models.TextField(null=True, blank=True, default=None)
    density               = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    image                 = models.ImageField(null=True, blank=True)
    # amount               = models.ForeignKey('Quantity', null=True, blank=True)
    # dimensions           = models.ForeignKey('', null=True, blank=False)
    sustainability_rating = models.IntegerField(null=True, blank=True)
    sustainability_notes  = models.TextField(null=True, blank=True, default=None)
    category              = models.CharField(max_length=20, choices=MATERIALS, blank=True)
    #electrical conductivity
    #thermal conductivity
    #poisson's ratio
    #youngs modulus
    
    # Properties of Wood #
    scientific_name       = models.CharField(max_length=30, null=True, blank=True, default=None)
    distribution          = models.CharField(max_length=30, null=True, blank=True, default=None)
    tree_height           = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    trunk_diameter        = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    average_dried_weight  = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    specific_gravity      = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    janka_hardness        = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    modulus_of_rupture    = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    elastic_modulus       = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    crushing_strength     = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    shrinkage             = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    endgrain              = models.TextField(null=True, blank=True, default=None)
    rot_resistance        = models.TextField(null=True, blank=True, default=None)
    workability           = models.TextField(null=True, blank=True, default=None)
    
    # Properties of Metals #
    molecular_weight      = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    CAS_number            = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    crystal_structure     = models.TextField(null=True, blank=True, default=None)
    modulus_of_elasticity = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    ferrous               = models.NullBooleanField(null=True, blank=True, default=None) #why is default false?
    electric_conductivity = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    ductility             = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    melting_point         = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    hardness              = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    thermal_condutivity   = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    refractive_index      = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    ionization_energy     = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    
    # Properties of Ceramics #
    
    # Properties of Semiconductors #
    
    # Properties of Polymers #
    
    # Properties of Composites #
    
    # Properties of Optical/Electromagnetic # i dont think that is a category of material. It a property
    
    objects    = MaterialManager()
            
class DirectionManager(models.Manager):
    """
    The DirectionManager class handles sets of directions for assembly, 
    disassembly and maintenance.
    """
    
    def create_direction(self, category, title, description, subtitle=None,  \
        image=None, drawing=None, next_dir=None, system=None, assembly=None):
        """
        """
        d = self.create()
        
        # Add required params
        d.category = category
        d.title    = title
        d.description = description
        
        # Add optional params
        d.subtitle = subtitle or None
        d.image = image or None
        d.drawing = drawing or None
        
        d.save()
        return d
        
    def set_ordered_list(self, usr_dir_list):
        """
        """
        for dir in iter(usr_dir_list):
            try:
                dir.next_dir = usr_dir_list.next
            except:
                return False
        return True

class Direction(models.Model):
    """
    A direction object ...
    """
    
    ASSEMBLE = 'A'
    DISSASEMBLE = 'D'
    MAINTAIN = 'M'
    
    CATEGORY_CHOICES = (    ('A','assemble'),
                            ('D','disassemble'),
                            ('M','maintain') )
    
    category     = models.CharField(max_length=20, choices=CATEGORY_CHOICES)
    title        = models.CharField(max_length=30)
    description  = models.TextField()
    subtitle     = models.CharField(max_length=30, null=True, blank=True)
    image        = models.FileField(null=True, blank=True)
    drawing      = models.FileField(null=True, blank=True)

    next_dir     = models.ForeignKey('self', related_name='+', null=True, blank=True)
    
    assembly     = models.ForeignKey('Assembly', null=True, blank=True)
    system       = models.ForeignKey('System', null=True, blank=True)
    component    = models.ForeignKey('Component', null=True, blank=True)
    
    objects   = DirectionManager()
    
    def __unicode__(self):
        return self.title

class PurposeManager(models.Manager):
    """
    """
    
    def create_purpose( self, purp_verb, purp_noun):
        """
        """
        p = self.create()
        
        p.purp_verb = purp_verb
        p.purp_noun = purp_noun
        
        p.save()
        return p
        
    def display_purp_meth(self, item):
        """
        Display the purpose and mehtod for the item (nr, sys or component)
        
        Ex. display_purp_meth(mySystem) will return 'generate_power_via_mechanical_rotation'
        """
        purposes = item.purpose_set.all()
        
        for purp in purposes:
            methods = purp.method_set.all()
        
            for method in methods:
                print purp.purp_verb, "_", purp.purp_noun, "_via_", method.method_noun,"_", method.method_adjective
        
    def is_valid(self):
        """
        Returns True if the purpose is valid.
        
        Validity Requirements:
            purp_verb
            purp_noun
            component or system: must belong to a component or system
        """
        
        # Check required variables
        if (self.purp_verb or self.purp_noun) == None:
            return False
            
        # Make sure that the instance of purpose belongs to a component or system
        # INCOMPLETE
        
        
        # Make sure that the instance of purpose has a valid method
        # INCOMPLETE
        
        return True

class Purpose(models.Model):
    """
    The Purpose class defines the purpose or function of a system. Each system has a single purpose.
    
    Each Purpose has a single method 
    """
    
    purp_verb     = models.CharField(max_length=20, choices=VERBS_LIST)
    purp_noun      = models.CharField(max_length=20, choices=NOUNS_LIST)
    
    component     = models.ManyToManyField('Component',null=True, blank=True)
    system        = models.ManyToManyField('System', null=True, blank=True)
    
    objects = PurposeManager()
    
    def __unicode__(self):
        return ''.join([self.purp_verb, '_' , self.purp_noun])

class MethodManager(models.Manager):
    """
    The Method Manager handles creation of methods by ensuring appropriate fields are entered
    
    Ex. create_method(  method_adjective='mechanical', method_noun='vibration', \
                        initial_force=Quantity.create_quantity([3], 'n')), ...
                        
    Ex. create_method(method_adjective='mechanical', method_noun='rotation', torque=Quantity.objects.create_quantity([75,0,0],'Nm', 'r'),)
    """
    
    def create_method(  self, method_adjective, method_noun, initial_force=None, frequency=None, \
                        mass=None, damping_ratio=None, amplitude=None, mode=None,  \
                        natural_frequency=None, type_title=None, description=None, torque=None, \
                        angular_velocity=None, purpose=None, component=None, system=None, natural_resource=None):
        """
        Create a new method with parameter based on the method verb and object
        """
        m = self.create()
        method_category = m.get_method()
        
        m.method_adjective = method_adjective 
        m.method_noun      = method_noun
        
        if purpose != None: m.purpose = purpose
        if component != None: m.component = component
        if system != None: m.system = system
        if natural_resource != None: m.natural_resource = natural_resource
        
        if method_category == 'mechanical_vibration':
            try:
                # Fill out parameters for Mech Vib
                m.initial_force     = initial_force
                m.frequency         = frequency
                m.mass              = mass
                m.damping_ratio     = damping_ratio
                m.amplitude         = amplitude
                m.mode              = mode
                m.natural_frequency = natural_frequency
               
                #ADD THESE WHEN DOING THE FORMULA FUNCTIONS
               
                # if( m.initial_force or m.frequency or m.mass or m.damping_ratio or m.amplitude or \
                #     m.mode or m.natural_frequency) == None:
                #     raise TypeError("All variables in list_method_properties() must be included")
            except:
                print "Method mechanical_vibration could not be created...."
        if method_category == 'mechanical_rotation':
            try:
                # Fill out parameters for Mech Rotation
                m.type_title        = type_title
                m.description       = description
                m.torque            = torque
                m.angular_velocity  = angular_velocity
                
                if( m.type_title or m.description or m.torque or m.angular_velocity) == None:
                    raise TypeError("All variables in list_method_properties() must be included")
            except:
                print "Method mechanical_rotation could not be created...."
            
        m.save()
        return m
        
    def is_valid(self):
        """
        Returns True if the method is valid.
        
        Validity Requirements:
            method_adjective
            method_noun
            purpose: must belong to a component or system
        """
        # Check required variables
        if (self.method_adjective or self.method_noun) == None:
            return False
            
        # Make sure that the instance of method belongs to a purpose
        # INCOMPLETE
        
        return True

class Method (models.Model):
    """
    The method for accomplishing a purpose
    
    Ex. vibrate metal, rotate blades, etc
    """

    method_adjective   = models.CharField(max_length=30, choices=ADJECTIVES_LIST)
    method_noun        = models.CharField(max_length=30, choices=NOUNS_LIST)
    purpose            = models.ManyToManyField('Purpose', null=True, blank=True)
    
    # Mechanical Vibration Method
    initial_force     = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    frequency         = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    mass              = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    damping_ratio     = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    amplitude         = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    mode              = models.DecimalField(max_digits=5, decimal_places=3, null=True, blank=True, default=None)
    natural_frequency = models.ForeignKey('Quantity', related_name='+', null= True, blank=True, default=None)
    
    # Mechanical Rotation Method
    type_title       = models.CharField(max_length=30)
    description      = models.TextField(null=True, blank=True, default=None)
    torque           = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    angular_velocity = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    radius           = models.ForeignKey('Quantity', related_name='+', null=True, blank=True, default=None)
    
    # Photoelectric Effect
    # rating          = 
    
    
    # irradiance
    # 
    
    objects = MethodManager()
    
    def get_method(self):
        """
        Gets the full verb_noun combination of the method
        """
        return '_'.join((self.method_noun, self.method_adjective))
        
    def add_purpose_to_method(self, p):
        """
        """
        self.purpose.add(p)
        return True
        
    def __unicode__(self):
        return self.get_method()