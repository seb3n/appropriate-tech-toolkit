"""
Author: Benoit Sebastien, Mckinnley Workman

File Description: 
   
"""

from django.shortcuts import render, render_to_response
from django.http import Http404, HttpResponse
from toolkit.models import *
from django.views.generic import CreateView

def energy_home(request):
    """
    Energy Home Page: Displays a list of natural resources to be used by the user, a map in which the user can select his location the purpose and the desired output of the assembly the user needs
    """

    # if request.method == 'GET':
    #     form = QuestionForm()
    
    # natural_resources = NaturalResource.objects.all() #a list of the natural resource objcts
    natural_resources  = ['sun', 'wind', 'rainfall', 'geothermal']
    outputs            = ['electricity','stored_power','mechanical_work','heating', 'cooling','water']
    
    context = { #Access variable with current category name
                'natural_resources' : natural_resources ,
                'outputs' : outputs
                # make a list of your natural res and give that list to the context
              }
    return render(request, "energy_home.html", context)

def water_home(request):
    context = { #"curr_category" : Category.objects.get(title="Water"),
                #"categories"    : Category.objects.all(),
                #"systems"       : System.objects.all(),
                # "questions"     : Question.objects.all(),
                #Add other water specific items
                }
    return render(request, "scroll.html", context)

def agriculture_home(request):
    context = { #"curr_category" : Category.objects.get(title="Agriculture"),
                #"categories"    : Category.objects.all(),
                #"systems"       : System.objects.all(),
                #"questions"     : Question.objects.all(),
                #Add other agriculture specific items
                }
    return render(request, "scroll.html", context)

def structure_home(request):
    context = { #"curr_category" : Category.objects.get(title="Structure"),
                #"categories"    : Category.objects.all(),
                #"systems"       : System.objects.all(),
                #"questions"     : Question.objects.all(),
                #Add other structure specific items
                }
    return render(request, "scroll.html", context)

def build_assembly(request):
    """
    Building Assembly Page
    """
    
    natural_resources = ['wind', 'rain', 'geothermal', 'solar', 'flowing-water']
    
    systems    = System.objects.all()
    components = Component.objects.all() 
    
    context = { #Access variable with current category name
                'natural_resources'     : natural_resources,
                'systems'               : systems,
                'components'            : components,
              }
    return render(request, "build_assembly.html", context)