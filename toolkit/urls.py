"""
Author: Benoit Sebastien, Mckinnley Workman

File Description: 
   
"""

from django.conf.urls import patterns, include, url
# from toolkit.views import SystemCreateView
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    #URL for the home.org/toolkit/energy page    
    url(r'^energy/','toolkit.views.energy_home',name='energy_home'),

    #URL for the home.org/toolkit/water page
    url(r'^water/', 'toolkit.views.water_home', name='water_home'),

    #URL for the home.org/toolkit/agriculture page
    url(r'^agriculture/', 'toolkit.views.agriculture_home', name='agriculture_home'),

    #URL for the home.org/toolkit/structure page
    url(r'^structure/', 'toolkit.views.structure_home', name='structure_home'),

    #URL for the home.org/toolkit/build_assembly paage
    url(r'^build_assembly/','toolkit.views.build_assembly',name='build_assembly'),

    # url(r'^add_system/', SystemCreateView.as_view()),

    # url(r'^$','energy.views.energy_home',name='energy_home'), #which is the correct syntax?

    # url(r'^blog/', include('blog.urls')),
)
